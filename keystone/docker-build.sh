
# replace the BUILD_PROXY build arg with a decent value for your build environment

docker build --build-arg BUILD_DATE=$(date -u +'%Y-%m-%dT%H:%M:%SZ') --build-arg BUILD_PROXY=http://alca.proxy.corp.sopra:8080 -t cas/keystone:0.0.1-SNAPSHOT .
