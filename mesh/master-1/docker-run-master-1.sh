#!/bin/sh


CONTAINER_NAME=master-1

CLUSTER_NAME=demo-mesh-cluster


docker run \
        --name $CONTAINER_NAME \
        --network host \
        -d \
        -v 'data':'/mesh/data/' \
        -v 'config':'/mesh/config/' \
        -e "MESH_UPDATECHECK=false" \
        -e "MESH_HTTP_HOST=192.168.56.101" \
        -e "MESH_HTTP_PORT=8080" \
        -e "MESH_CLUSTER_ENABLED=true" \
        -e "MESH_CLUSTER_NAME=$CLUSTER_NAME" \
        -e "MESH_NODE_NAME=$CONTAINER_NAME" \
        -e "MESH_ELASTICSEARCH_START_EMBEDDED=false" \
        -e "MESH_ELASTICSEARCH_URL=http://192.168.56.101:9200" \
        gentics/mesh
