#!/bin/sh

export MESH_HTTP_HOST=192.168.56.101

export MESH_HTTP_PORT=8080

export MESH_CLUSTER_ENABLED=false

export MESH_UPDATECHECK=false

export MESH_ELASTICSEARCH_START_EMBEDDED=false

export MESH_ELASTICSEARCH_URL=http://192.168.56.101:9200

java -jar ../mesh-server-0.7.1.jar

