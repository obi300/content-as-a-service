package com.isban.content_management.hippo_cms.beans;

import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoDocument;

@Node(jcrType="myhippoproject:basedocument")
public class BaseDocument extends HippoDocument {

}
