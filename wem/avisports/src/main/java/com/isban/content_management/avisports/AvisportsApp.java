package com.isban.content_management.avisports;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class AvisportsApp {


    public static void main( String... args ) {

        SpringApplication.run( AvisportsApp.class, args );
    }
}
