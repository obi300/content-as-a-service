require.config({
	paths: {
		'jquery': 'libs/jquery/jquery.min',
		'backbone': 'libs/backbone/backbone.min',
		'underscore': 'libs/underscore/underscore.min',
		'templates': '../templates'
	},
	shim: {
		'underscore': {
			exports: '_'
		},
		'backbone': {
			deps: ["underscore" , "jquery"],
			exports: 'Backbone'
		}
	}
});

require(
	[ "approuter" , "appconfig" ],
	
	function(AppRouter , appconfig) {
		appRouter = new AppRouter(appconfig);
		
	}
);

