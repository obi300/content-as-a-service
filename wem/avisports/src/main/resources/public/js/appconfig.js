define(['jquery' , 'backbone' , 'text!templates/HomeLayout1.html' , 'text!templates/SectionLayout3.html', 'text!templates/SectionLayout4.html',
					'text!templates/SectionLayoutGreen.html' , 'text!templates/SectionLayoutTargeting.html',
					'text!templates/SectionLayoutOrange.html' ], function($ , Backbone , HomeLayout1, SectionLayout3 , SectionLayout4 , 
					SectionLayoutGreen , SectionLayoutTargeting , SectionLayoutOrange) {


	/**** CONFIGURABLES  Change according to your settings****/
	SITES_HOST_NAME = 'localhost'; // Sites server host name

	SITES_PORT = '8003'; // Sites server Port

	SITES_CONTEXT = 'sites'; // Sites context

	SITENAME = 'avisports';

	$.ajaxPrefilter( function( options , originalOptions , jqXHR ) {
		if(options.url.search("http") == -1){
			if(options.category == "search")
				options.url = 'http://' + SITES_HOST_NAME + ':' + SITES_PORT + '/' + SITES_CONTEXT + '/REST/resources/v1/search/sites/'+ SITENAME + options.url;
			else if(options.category = "aggregates")
				options.url = 'http://' + SITES_HOST_NAME + ':' + SITES_PORT + '/' + SITES_CONTEXT + '/REST/resources/v1/aggregates/'+ SITENAME + options.url;
		}			
	});


	var sync = Backbone.sync;

	Backbone.sync = function(method, model, options) {
		options.beforeSend = function (xhr) {
			xhr.setRequestHeader('Pragma', 'auth-redirect=false');
			xhr.setRequestHeader('Accept','application/json');
		};

		// Update other options here.

		sync(method, model, options);
	};

	processEmbeddedLinks = function(text){
		return text.replace(/href/, 'href1');
	};

	getFullImageLink = function(link){
		return 'http://' + SITES_HOST_NAME + ':' + SITES_PORT + '/' + link;
	};


	return {

		"templates": {
			1327351719456	:  { "name" : "HomeLayout1", "template": HomeLayout1 },
			1329851332601	:  { "name" : "SectionLayoutGreen" , "template": SectionLayoutGreen },
			1329326970440	:  { "name" : "SectionLayout4" , "template" : SectionLayout4 },
			1361217259137	:  { "name" : "SectionLayoutTargeting" , "template" : SectionLayoutTargeting },
			1327351719467	:  { "name" : "SectionLayoutOrange" , "template" : SectionLayoutOrange },
			1327351719525	:  { "name" : "SectionLayout3" , "template" : SectionLayout3 }
		},

		"templateParams": {

			"HomeLayout1"	: { assetDepth : "1" , fields : "Page(banner,teaserImages,teaserText,bannerText);AVIImage(imageFile,width,height)" , expand : "Page,AVIImage"},

			"SectionLayoutGreen"	: { assetDepth : "2" , 
										fields : "AVIArticle(id,relatedImage,abstract,headline);Page(banner,titleContent1,titleContent2,Assoc_Named_contentList1,Assoc_Named_contentList2);AVIImage(imageFile,smallThumbnail,largeThumbnail)" , 
										expand : "Page,AVIImage,AVIArticle" 
										
									  },

			"SectionLayout4"	: { 	assetDepth : "2" , 
										fields : "AVIArticle(id,relatedImage,abstract,headline);Page(banner,titleContent1,titleContent2,Assoc_Named_contentList1,Assoc_Named_contentList2);AVIImage(imageFile,smallThumbnail,largeThumbnail);YouTube(externalid)" , 
										expand : "Page,AVIImage,AVIArticle,YouTube" 
								  },

			"SectionLayoutTargeting"	: { assetDepth : "1" , 
											fields : "AVIArticle(id,relatedImage,abstract,headline);Page(banner,recommendation);AVIImage(imageFile,smallThumbnail,largeThumbnail);AdvCols(items)" ,
											expand : "Page,AVIImage,AdvCols,AVIArticle"  
										  },

			"SectionLayoutOrange"	: { 	assetDepth : "2" , 
											fields : "AVIArticle(id,relatedImage,abstract,headline);Page(banner,titleContent1,titleContent2,Assoc_Named_contentList1,Assoc_Named_contentList2);AVIImage(imageFile,smallThumbnail,largeThumbnail)" , 
											expand : "Page,AVIImage,AVIArticle" 
								  	  },

			"SectionLayout3"	:    { 		assetDepth : "2" , 
											fields : "AVIArticle(id,relatedImage,abstract,headline,relatedStories);Page(banner,titleContent1,titleContent2,Assoc_Named_contentList1,Assoc_Named_contentList2);AVIImage(imageFile,smallThumbnail,largeThumbnail,sidebarThumbnail)" , 
											expand : "Page,AVIImage,AVIArticle" 
								  	 }

		}
	};

	
	
});
