define(['backbone' , 'jquery' , 'underscore' , 'text!templates/articlesList.html'], function(Backbone , $ , _ , ArticleListTemplate ) {
	var ArticleListContentView = Backbone.View.extend({
		el: '#content-zone',
	
		initialize: function(articleListData) {
			this.model = articleListData;
		},
		
		render : function() {
			var articleListContentHTML = _.template( ArticleListTemplate , {articleListData : this.model}) ;
			this.$el.html(articleListContentHTML);
		}
		
	});
	
	return ArticleListContentView;
});	