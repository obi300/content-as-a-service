define(['backbone' , 'jquery' , 'underscore'], function(Backbone , $ , _ ) {
	var PageContentView = Backbone.View.extend({
		el: '#content-zone',
	
		initialize: function(aggrPageData , template) {
			this.model = aggrPageData;
			this.template = template;
		},
		
		render : function() {
			var pageContentHTML = _.template( this.template , {aggrPageData : this.model}) ;
			this.$el.html(pageContentHTML);
		}
		
	});
	
	return PageContentView;
});	