define(['backbone' , 'jquery' , 'underscore' , 'text!templates/RESTException.html'], function(Backbone , $ , _  , RESTExceptionTemplate ) {
	var RESTExceptionView = Backbone.View.extend({
		el: '#content-zone',
	
		initialize: function(RESTExceptionData) {
			this.model = RESTExceptionData;
		},
		
		render : function() {
			var pageContentHTML = _.template( RESTExceptionTemplate , {RESTException : this.model}) ;
			this.$el.html(pageContentHTML);
		}
		
	});
	
	return RESTExceptionView;
});	