define(['backbone' , 'jquery' , 'underscore' , 'text!templates/footer.html'], function(Backbone , $ , _ , FooterTemplate) {
	var HeaderView = Backbone.View.extend({
		el: '#footer-zone',
		
		render : function() {
			var footerHTML = _.template( FooterTemplate ) ;
			this.$el.html(footerHTML);
		}
		
	});
	
	return HeaderView;
});	