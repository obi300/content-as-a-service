define(['backbone' , 'jquery' , 'underscore' , 'text!templates/articlesSearchList.html'], function(Backbone , $ , _ , ArticleSearchListTemplate ) {
	var ArticleSearchListContentView = Backbone.View.extend({
		el: '#content-zone',
	
		initialize: function(articleSearchListData) {
			this.model = articleSearchListData;
		},
		
		render : function() {
			var articleSearchListContentHTML = _.template( ArticleSearchListTemplate , {articleSearchListData : this.model.get('items')}) ;
			this.$el.html(articleSearchListContentHTML);
		}
		
	});
	
	return ArticleSearchListContentView;
});	