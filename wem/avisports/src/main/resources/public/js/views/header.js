define(['backbone' , 'jquery' , 'underscore' , 'text!templates/header.html'], function(Backbone , $ , _ , HeaderTemplate) {
	var HeaderView = Backbone.View.extend({
		el: '#header-zone',
	
		initialize: function(aggrSiteNavigationData) {
			this.model = aggrSiteNavigationData;
		},
		
		render : function() {
			var headerHTML = _.template( HeaderTemplate , {siteNavigationData : this.model}) ;
			this.$el.html(headerHTML);
		},

		events : {
			'change #dropdownList' : 'f',
			'click #searchButton' : 'search',
			'keyup #searchText' : 'keyaction'
		},

		f : function(ev){
			var selectedIndex = ev.target.selectedIndex;
			var segment = ev.target[selectedIndex].value;
			var url = '';
			/*if( segment != 'default')
			{
				url = Backbone.history.fragment.
			}*/
			window.location.href = '#' + Backbone.history.fragment + '/' +  segment;
		} ,

		search : function(){
			var str = document.getElementById('searchText').value;
			this.redirectToRouter(str);
		},

		keyaction : function(ev){
			/*if(ev.keyCode == 13)
			{*/
				var str = ev.target.value;
				//ev.target.value = '';
				this.redirectToRouter(str);
			//}
		},

		redirectToRouter : function(str){
			if(str.length != 0)
				window.location.href = '#search/articles/' + str;
		}

	});
	
	return HeaderView;
});	