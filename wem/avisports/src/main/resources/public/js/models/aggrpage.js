define(['backbone'], function(Backbone) {
	var AggrPage = Backbone.Model.extend({
						urlRoot:'/Page'
					});
	return AggrPage; 				
});