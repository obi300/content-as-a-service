define(['backbone' , 'jquery' , 'underscore' , 'text!templates/article.html'], function(Backbone , $ , _ , ArticleTemplate ) {
	var ArticleContentView = Backbone.View.extend({
		el: '#content-zone',
	
		initialize: function(aggrArticleData) {
			this.model = aggrArticleData;
		},
		
		render : function() {
			var articleContentHTML = _.template( ArticleTemplate , {aggrArticleData : this.model}) ;
			this.$el.html(articleContentHTML);
		}
		
	});
	
	return ArticleContentView;
});	