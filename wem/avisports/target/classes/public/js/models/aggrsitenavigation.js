define(['backbone'], function(Backbone) {
	var AggrSiteNavigation = Backbone.Model.extend({
						urlRoot:'/navigation'
					});
	return AggrSiteNavigation; 				
});