define(['backbone'], function(Backbone) {
	var AggrPage = Backbone.Model.extend({
						urlRoot:'/ContentQuery',
						url : function(){
							if(this.id.search("http") != -1)
								return this.id;
							return this.urlRoot + "/" + this.id + "/items";	

						}
								
					});
	return AggrPage; 				
});