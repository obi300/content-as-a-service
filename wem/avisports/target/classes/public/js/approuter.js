define(['backbone' , 'models/aggrsitenavigation' , 'models/aggrpage' , 'models/aggrarticle', 'models/articleslist' , 'models/articlesSearchList', 'views/header' , 'views/pagecontent' , 'views/footer' , 'views/articlecontent' , 'views/articlesList' , 'views/articlesSearchList', 'views/RESTException'], 
					function(Backbone , AggrSiteNavigation , AggrPage , AggrArticle , ArticlesList, ArticlesSearchList, HeaderView , PageContentView , FooterView , ArticleView , ArticlesListView , ArticlesSearchListView, RESTExceptionView) {
							 
	var AppRouter = Backbone.Router.extend({
		
		initialize:function(appConfig){

			this.appConfig = appConfig;
			var that = this;
			this.paginationLimit = "8";
			this.articlesContentQueryId = "1395380847207";
			this.aggrSiteNavigationDataModel = new AggrSiteNavigation({id:"Default"});
			this.aggrSiteNavigationDataModel.fetch({
				data: {
							assetDepth : "0",
							fields : "Page(children,name,id);SiteNavigation(children)",
							expand : "Page"
					  },
				processData : true ,
				
				category : "aggregates",

				success: function ( aggrSiteNavigationDataModel ) {
					var start = aggrSiteNavigationDataModel.get('start');
					var homePageNavigationMap = aggrSiteNavigationDataModel.get(start).children[0];
					var headerView = new HeaderView(homePageNavigationMap[homePageNavigationMap.start]);
					headerView.render();
					Backbone.history.start();
				},

				error: function( httpresponse , restException) {
					that.displayErrorPage(restException);
				}
			});
		},
		
		routes : {

			'' : 'homePage',
			':name' : 'Page',
			'article/:id' : 'displayArticle',
			'ContentQuery/articles' : 'displayStartPageArticles',
			'ContentQuery/articles/paging/:pageNo' : 'pagingController',
			'search/articles/:searchKey' : 'displaySearchResults'
		},
		
		displaySearchResults : function(searchKey){
			var that = this;
			this.articlesSearchListModel = new ArticlesSearchList();

			this.articlesSearchListModel.fetch({
				data : {
							"field:name:startswith": searchKey
					  },
				processData : true ,

				category : "search",
				
				success: function ( articlesSearchListModel ) {
					var articlesSearchListView = new ArticlesSearchListView(articlesSearchListModel);
					articlesSearchListView.render();
					var footerView = new FooterView();
					footerView.render();
				},

				error: function( httpresponse , restException) {
					that.displayErrorPage(restException);
				}
			});
		},

		homePage : function() {
			
			this.Page("Home");
		
		},
		
		Page: function(name) {
			var that = this;
			var id = this.getId_Of(name);
			this.aggrPageDataModel = new AggrPage({id:id});
			var templateInfo = this.appConfig.templates[id];
			this.aggrPageDataModel.fetch({
				data : this.appConfig.templateParams[templateInfo["name"]],
					
				processData : true ,
				
				category : "aggregates",
				 
				success: function ( aggrPageDataModel ) {
					var pageContentView = new PageContentView(aggrPageDataModel , templateInfo["template"]);
					pageContentView.render();
					var footerView = new FooterView();
					footerView.render();
				},

				error: function( httpresponse , restException) {
					that.displayErrorPage(restException);
				}
			});
					
		},

		getId_Of: function(name) {

			var start = this.aggrSiteNavigationDataModel.get('start');
			var aggrSiteNavigationData = this.aggrSiteNavigationDataModel.get(start);

			var homePageDataMap = aggrSiteNavigationData.children[0];
			var homePageData = homePageDataMap[homePageDataMap.start];

			if(name == "Home")
				return homePageData.id;

			for(var i = 0 ; i < homePageData.children.length ; i++)
			{
				var child = homePageData.children[i];
				if(child[child.start].name == name)
					return child[child.start].id;
			}
			
		},

		getIndexOf: function(rel){

			var articlesListLinks = this.articlesListModel.get('links');
			for(var i = 0 ; i < articlesListLinks.length ; i++)
			{
				if(articlesListLinks[i].rel == rel)
					return i;
			}

			return -1;

		},


		displayArticle: function(id){
			var that = this;
			this.aggrArticleDataModel = new AggrArticle({id:id});
			this.aggrArticleDataModel.fetch({
				data: {
							assetDepth : "2",
							fields: "category,author,postDate,subheadline,relatedLinks,relatedStories,relatedImage,body;AVIArticle(id,headline,abstract,relatedImage);AVIImage(imageFile,caption,width,height,sidebarThumbnail)",
							expand: "AVIArticle,AVIImage"

					  },
				processData : true ,

				category : "aggregates",	
				
				success: function ( aggrArticleDataModel ) {
					var articleView = new ArticleView(aggrArticleDataModel);
					articleView.render();
					var footerView = new FooterView();
					footerView.render();
				},

				error: function( httpresponse , restException) {
					that.displayErrorPage(restException);
				}
			});

		},

		pagingController: function(pageNo){

			if(this.currentPageNo + 1 == pageNo)
				this.articlesListModel = new ArticlesList({id:this.articlesListModel.get('links')[this.getIndexOf("next")].href});
			else if(this.currentPageNo  - 1 == pageNo)
				this.articlesListModel = new ArticlesList({id:this.articlesListModel.get('links')[this.getIndexOf("prev")].href});
			else if(this.currentPageNo && pageNo == 0)
				this.articlesListModel = new ArticlesList({id:this.articlesListModel.get('links')[this.getIndexOf("first")].href});
			else if(this.currentPageNo && pageNo == this.lastPageNo)
				this.articlesListModel = new ArticlesList({id:this.articlesListModel.get('links')[this.getIndexOf("last")].href});
			else {
					this.displayPageArticlesFromPageNo(pageNo);
					return;
			}

			this.displayPageArticlesFromLinks();

		},

		displayPageArticlesFromPageNo: function(pageNo) {

			var that = this;
			this.articlesListModel = new ArticlesList({id:this.articlesContentQueryId});
			this.articlesListModel.fetch({
				data: { 
						 	assetDepth : "1",
						 	fields: "AVIArticle(id,headline,abstract,relatedImage);AVIImage(imageFile,smallThumbnail)",
							expand: "AVIArticle,AVIImage",
							offset : pageNo * this.paginationLimit,
							limit : this.paginationLimit

					  },
				processData : true ,
				
				category : "aggregates",

				success: function ( articleslist ) {
					that.currentPageNo = pageNo; 
					that.lastPageNo = Math.ceil(articleslist.get('limit')/that.paginationLimit) - 1;
					var articlesListView = new ArticlesListView(articleslist);
					articlesListView.render();
					var footerView = new FooterView();
					footerView.render();
				},

				error: function( httpresponse , restException) {
					that.displayErrorPage(restException);
				}
			});

		},

		displayPageArticlesFromLinks: function() {
			var that = this;
			this.articlesListModel.fetch({
				success: function ( articleslist) {
						var articlesListView = new ArticlesListView(articleslist);
						that.currentPageNo = articleslist.get('offset') / that.paginationLimit; 
						articlesListView.render();
						var footerView = new FooterView();
						footerView.render();
				},

				error: function( httpresponse , restException) {
					that.displayErrorPage(restException);
				}	
			});

		},

		displayStartPageArticles: function(){

			this.displayPageArticlesFromPageNo("0");				

		},

		displayErrorPage: function(restException){
			var restExceptionView = new RESTExceptionView(restException.responseJSON);
			restExceptionView.render();
			var footerView = new FooterView();
			footerView.render();
		}

		
	});
	
	return AppRouter;
	
});
	