import { Component, OnInit } from '@angular/core';
import { TicketService } from './common/ticket.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {


  constructor( private ticketService: TicketService ) {}


  ngOnInit() {

    this.ticketService.globalTicket = window['globalTicket'];
  }

}
