import { Injectable } from '@angular/core';

@Injectable()
export class TicketService {

  private _globalTicket: string;

  constructor() { }

  get globalTicket(): string {
    return this._globalTicket;
  }

  set globalTicket( value: string ) {
    this._globalTicket = value;
  }
}
