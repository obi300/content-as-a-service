import { Component, OnDestroy, OnInit } from '@angular/core';
import { SitesService } from './sites.service';
import { Subscription } from 'rxjs/Subscription';


@Component({
  selector: 'app-sites',
  templateUrl: './sites.component.html',
  styleUrls: ['./sites.component.css']
})
export class SitesComponent implements OnInit, OnDestroy {


  private _sites: Object[];

  private findAllSitesSubscription: Subscription;


  constructor( private sitesService: SitesService ) {

    this._sites = [];
  }


  ngOnInit() {
  }


  ngOnDestroy() {

    if( this.findAllSitesSubscription ) {

      this.findAllSitesSubscription.unsubscribe();
    }
  }


  public loadSites(): void {

    if( !this.findAllSitesSubscription ) {

      this.findAllSitesSubscription = this.sitesService.findAllSites().subscribe(
        ( sites: Object[] ) => {
          this._sites = sites;
        } );
    }
  }


  get sites(): Object[] {
    return this._sites;
  }

}
