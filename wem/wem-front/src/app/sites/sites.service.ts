import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BaseService } from '../common/base-service';


@Injectable()
export class SitesService extends BaseService {


  constructor( private httpClient: HttpClient ) {
    super();
  }


  public findAllSites(): Observable< Object[] > {

    return this.httpClient.get< Object[] >( SitesService.BASE_ENDPOINT  );
  }

}
