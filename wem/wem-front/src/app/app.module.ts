import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SitesComponent } from './sites/sites.component';
import { UsersComponent } from './users/users.component';
import { SitesService } from './sites/sites.service';
import { HttpClientModule } from '@angular/common/http';
import { AssetTypesComponent } from './asset-types/asset-types.component';
import { AssetTypesService } from './asset-types/asset-types.service';
import { TicketService } from './common/ticket.service';

@NgModule({
  declarations: [
    AppComponent,
    SitesComponent,
    UsersComponent,
    AssetTypesComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [
    TicketService,
    AssetTypesService,
    SitesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
