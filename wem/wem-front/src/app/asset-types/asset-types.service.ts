import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders, HttpParams, HttpRequest } from '@angular/common/http';
import { BaseService } from '../common/base-service';
import { AssetType } from './asset-type';
import { TicketService } from '../common/ticket.service';
import 'rxjs/Rx';


@Injectable()
export class AssetTypesService extends BaseService {


  constructor( private ticketService: TicketService, private httpClient: HttpClient ) {
    super();
  }


  public findAll(): Observable< AssetType[] > {


    const path = BaseService.ASSET_TYPES_ENDPOINT;

    let headers =
      new HttpHeaders()
        .set( 'Accept', 'application/json' )
        .set( 'Pragma', 'auth-redirect=false' )
        .set( 'X-CSRF-Token', this.ticketService.globalTicket );

//    X-CSRF-Token

    const params = new HttpParams();

    params.set( 'ticket', this.ticketService.globalTicket );

    const req = new HttpRequest( 'GET', path, null, { headers: headers } );

    return this
            .httpClient
            .request( req )
            .map(
              ( json: Object ) => {

                // this is due to the fact that Firebase does not use arrays
                const assetTypes: AssetType[] = [];

                return assetTypes;
              }
            );
  }

}
