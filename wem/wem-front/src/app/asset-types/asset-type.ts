export class AssetType {

  private _name: string;

  private _href: string;

  private _subtypes: string[];


  public get name(): string {
    return this._name;
  }

  public set name( value: string ) {
    this._name = value;
  }

  public get href(): string {
    return this._href;
  }

  public set href( value: string ) {
    this._href = value;
  }

  public get subtypes(): string[] {
    return this._subtypes;
  }

  public set subtypes( value: string[] ) {
    this._subtypes = value;
  }

}
