import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { AssetTypesService } from './asset-types.service';
import { AssetType } from './asset-type';


@Component({
  selector: 'app-asset-types',
  templateUrl: './asset-types.component.html',
  styleUrls: ['./asset-types.component.css']
})
export class AssetTypesComponent implements OnInit, OnDestroy {


  private _assetTypes: AssetType[];

  private assetTypesSubscription: Subscription;


  constructor( private assetTypesService: AssetTypesService ) {

    this._assetTypes = [];
  }


  ngOnInit() {
  }


  ngOnDestroy() {

    if( this.assetTypesSubscription ) {

      this.assetTypesSubscription.unsubscribe();
    }
  }


  public loadAssetTypes(): void {

    if( !this.assetTypesSubscription ) {

      this.assetTypesSubscription = this.assetTypesService.findAll().subscribe(
        ( assetTypes: AssetType[] ) => {

          this._assetTypes = assetTypes;
        } );
    }
  }


  get assetTypes(): Object[] {

    return this._assetTypes;
  }

}
