import { TestBed, inject } from '@angular/core/testing';

import { AssetTypesService } from './asset-types.service';

describe('AssetTypesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AssetTypesService]
    });
  });

  it('should be created', inject([AssetTypesService], (service: AssetTypesService) => {
    expect(service).toBeTruthy();
  }));
});
