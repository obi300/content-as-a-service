import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';

import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { fromPromise } from 'rxjs/observable/fromPromise';

/**
 * Intercepts requests targeted at api servers.
 *
 * @author Produban
 */
@Injectable()
export class AppCustomInterceptor implements HttpInterceptor {


  constructor( private injector: Injector ) { }


  intercept( request: HttpRequest<any>, next: HttpHandler): Observable< HttpEvent< any > > {

    // let's ignore the diverse internationalization responses es.json, en.json, etc...

    let theRequest = request;

    return fromPromise( this.tokenApiService.getTicket() )
      .switchMap( apiToken => {

        theRequest = request.clone({ headers: request.headers.set( 'Authorization', 'Bearer ' + apiToken.accessToken ) } );

        return next
          .handle( theRequest )
          .catch( ( err: any ) => {

            console.log( err );

            return Observable.throw( err );
          })
          ;
      });
  }

}
