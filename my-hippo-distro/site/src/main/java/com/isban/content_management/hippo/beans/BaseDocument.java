package com.isban.content_management.hippo.beans;

import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoDocument;

@Node(jcrType="myhippodistro:basedocument")
public class BaseDocument extends HippoDocument {

}
