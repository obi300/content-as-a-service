#!/bin/sh

CONTAINER_NAME=es-data-1

CLUSTER_NAME=the-es-cluster

docker run \
        --name $CONTAINER_NAME \
        --ulimit memlock=-1:-1 \
        --network host \
        -d \
        -e "node.name=$CONTAINER_NAME" \
        -e "cluster.name=$CLUSTER_NAME" \
        -e "node.master=false" \
        -e "node.data=true" \
        -e "search.remote.connect=false" \
        -e "bootstrap.memory_lock=true" \
        -e "discovery.zen.ping.unicast.hosts=192.168.56.101" \
        -e "ES_JAVA_OPTS=-Xms1024m -Xmx1024m" \
        docker.elastic.co/elasticsearch/elasticsearch:6.2.2
