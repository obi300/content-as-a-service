#!/bin/sh

CONTAINER_NAME=es-master

CLUSTER_NAME=the-es-cluster

docker run \
        --name $CONTAINER_NAME \
        --ulimit memlock=-1:-1 \
        --network host \
        -d \
        -e "node.name=$CONTAINER_NAME" \
        -e "node.master=true" \
        -e "node.data=false" \
        -e "search.remote.connect=false" \
        -e "cluster.name=$CLUSTER_NAME" \
        -e "bootstrap.memory_lock=true" \
        -e "ES_JAVA_OPTS=-Xms512m -Xmx512m" \
        docker.elastic.co/elasticsearch/elasticsearch:6.2.2
