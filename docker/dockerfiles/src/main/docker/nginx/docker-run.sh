#!/bin/sh


docker run \
        --name nginx \
        --network host \
        -d \
        -v /opt/content-as-a-service/docker/dockerfiles/src/main/docker/nginx/nginx.conf:/etc/nginx/nginx.conf:ro \
        nginx
