
create user 'hippo'@'%' identified by 'hippo';

GRANT ALL PRIVILEGES ON hippo.* TO 'hippo'@'%' IDENTIFIED BY 'hippo' WITH GRANT OPTION;

flush privileges;
