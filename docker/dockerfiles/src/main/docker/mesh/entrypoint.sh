#!/bin/sh


MESH_MODE=$1


function runMesh() {

    $JAVA_HOME/bin/java -jar $MESH_DIR/$MESH_VERSION.jar
}


function startMeshInSingleMode() {

    cd $MESH_DIR

    export MESH_UPDATECHECK=false

    export MESH_HTTP_HOST=$HTTP_HOST_MESH

    export MESH_HTTP_PORT=$HTTP_PORT_MESH

    export MESH_CLUSTER_ENABLED=false

    export MESH_ELASTICSEARCH_START_EMBEDDED=false

    export MESH_ELASTICSEARCH_URL=$ELASTICSEARCH_URL_MESH

    runMesh
}


function startMeshInClusterMode() {

    cd $MESH_DIR

    export MESH_UPDATECHECK=false

    export MESH_HTTP_HOST=$HTTP_HOST_MESH

    export MESH_HTTP_PORT=$HTTP_PORT_MESH

    export MESH_CLUSTER_ENABLED=true

    export MESH_CLUSTER_NAME=$CLUSTER_NAME_MESH

    export MESH_NODE_NAME=$NODE_NAME_MESH

    export MESH_ELASTICSEARCH_START_EMBEDDED=false

    export MESH_ELASTICSEARCH_URL=$ELASTICSEARCH_URL_MESH

    runMesh
}


if [ "cluster" == "$MESH_MODE" ]
then
    echo "Launching Mesh in cluster mode."

    startMeshInClusterMode
else
    if [ ! -d "$MESH_DIR/config" ]
    then

        echo "Launching Mesh in single mode."

        startMeshInSingleMode
    else

        echo "Launching Mesh in clustered mode."

        startMeshInClusterMode
    fi
fi
