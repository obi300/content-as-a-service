#!/bin/sh

docker build \
       --build-arg BUILD_DATE=$(date -u +'%Y-%m-%dT%H:%M:%SZ') \
       --build-arg BUILD_VERSION=0.0.1 \
       -t cas/mesh:0.0.1 .
