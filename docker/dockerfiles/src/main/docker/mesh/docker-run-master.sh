#!/bin/sh


CONTAINER_NAME=$1

HTTP_HOST_MESH=$2

HTTP_PORT_MESH=$3

ELASTICSEARCH_URL_MESH=$4

MESH_MODE=$5


docker run \
        --name $CONTAINER_NAME \
        --network host \
        -d \
        -e "HTTP_HOST_MESH=$HTTP_HOST_MESH" \
        -e "HTTP_PORT_MESH=$HTTP_PORT_MESH" \
        -e "CLUSTER_NAME_MESH=some-mesh-cluster" \
        -e "NODE_NAME_MESH=$CONTAINER_NAME" \
        -e "ELASTICSEARCH_URL_MESH=$ELASTICSEARCH_URL_MESH" \
        cas/mesh:0.0.1 $MESH_MODE

