#!/bin/sh


docker build \
       --build-arg BUILD_DATE=$(date -u +'%Y-%m-%dT%H:%M:%SZ') \
       --build-arg BUILD_PROXY=http://alca.proxy.corp.sopra:8080 \
       -t cas/mesh .
