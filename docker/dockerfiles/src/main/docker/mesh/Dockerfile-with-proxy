FROM centos:7


# set build time args
ARG BUILD_DATE

ARG BUILD_PROXY


# set labels for the image ( see http://label-schema.org for more info )
LABEL org.label-schema.schema-version="1.0" \
      org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.name="cas/mesh" \
      org.label-schema.description="This image contains a simple version of Mesh CMS" \
      org.label-schema.vendor="Isban Global"


# set Java environment variables
ENV JDK_VERSION zulu8.27.0.7-jdk8.0.162-linux_x64

ENV JDK_URL https://cdn.azul.com/zulu/bin/$JDK_VERSION.tar.gz

ENV JDK_DIR /opt/java

ENV JAVA_HOME $JDK_DIR/$JDK_VERSION


# set Mesh environment variables
ENV MESH_VERSION  mesh-server-0.17.1

ENV MESH_URL  https://maven.gentics.com/maven2/com/gentics/mesh/mesh-server/0.17.1/MESH_VERSION.jar

ENV MESH_DIR  /opt/mesh

ENV HTTP_HOST_MESH localhost

ENV HTTP_PORT_MESH 8080

ENV CLUSTER_NAME_MESH some-mesh-cluster

ENV NODE_NAME_MESH some-node-name

ENV ELASTICSEARCH_URL_MESH  http://localhost:9200


# update os dependencies
RUN export http_proxy=$BUILD_PROXY \
    && yum update -y \
    && yum clean all \
    && rm -rf /var/cache/yum


# create directory structure
RUN mkdir -p $JDK_DIR \
    && mkdir -p $MESH_DIR


# download the jdk and uncompress it
RUN curl $JDK_URL > $JDK_DIR/jdk.tar.gz \
    && cd $JDK_DIR \
    && gunzip jdk.tar.gz \
    && tar -xvf jdk.tar \
    && chmod +x $JDK_DIR/* -R \
    && rm jdk.tar


# download mesh
RUN curl $MESH_URL > $MESH_DIR/$MESH_VERSION.jar


# entrypoint script
COPY entrypoint.sh  $MESH_DIR/entrypoint.sh


# set as executable all files under the mesh dir
RUN chmod +x $MESH_DIR/*


# set the entry point
ENTRYPOINT [ "entrypoint.sh" ]

# set the default mode of operation to the cluster mode
CMD [ "cluster" ]