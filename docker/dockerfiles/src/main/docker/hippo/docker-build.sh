#!/bin/sh

docker build \
       --build-arg BUILD_DATE=$(date -u +'%Y-%m-%dT%H:%M:%SZ') \
       --build-arg BUILD_PROXY=http://alca.proxy.corp.sopra:8080 \
       --build-arg BUILD_VERSION=0.0.1 \
       -t cas/hippo:0.0.1 .

