#!/bin/sh


function downloadProject() {

    cd $HIPPO_DIR/target/tomcat8x

    curl --noproxy 192.168.56.101, $HIPPO_PROJECT_URL > $HIPPO_PROJECT-$HIPPO_PROJECT_VERSION.tar.gz

    gzip -d $HIPPO_PROJECT-$HIPPO_PROJECT_VERSION.tar.gz

    tar -xvf $HIPPO_PROJECT-$HIPPO_PROJECT_VERSION.tar

    rm $HIPPO_PROJECT-$HIPPO_PROJECT_VERSION.tar

    chmod +x $HIPPO_DIR/* -R
}


function startHippo() {

    cd $HIPPO_DIR

    mvn -Pcargo.run -Drepo.path=storage
}


downloadProject

startHippo
